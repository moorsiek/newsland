from flask import Flask
from flask import render_template
from flask import send_from_directory

app = Flask(__name__)
app.debug = True

@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('static/js', path)
@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('static/css', path)
@app.route('/images/<path:path>')
def send_image(path):
    return send_from_directory('static/images', path)
@app.route('/')
def hello_world():
    comments = [
        {
            'children': [
                {
                    'children': []
                },
                {
                    'children': []
                }
            ]
        },
        {
            'children': [
                {
                    'children': []
                },
                {
                    'children': [
                        {
                            'children': [
                                {
                                    'children': [
                                        {
                                            'children': [
                                                {
                                                    'children': [
                                                        {
                                                            'children': [
                                                            
                                                            ]
                                                        },
                                                        {
                                                            'children': [
                                                                {
                                                                    'children': [
                                                                        {
                                                                            'children': [
                                                                                {
                                                                                    'children': [
                                                                                        {
                                                                                            'children': [
                                                                                            
                                                                                            ]
                                                                                        }
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            'children': [
                                                {
                                                    'children': [
                                                    
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            'children': []
                        }
                    ]
                }
            ]
        },
        {
            'children': []
        }
    ]

    return render_template('comments.html', comments=comments)
    #return 'Hello World!'

if __name__ == '__main__':
    app.run()


