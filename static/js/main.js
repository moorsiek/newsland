$(document).ready(function(){	
	//placeholder
	$('[placeholder]').focus(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
            input.val('');
            input.removeClass('placeholder');
        }
    }).blur(function() {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.addClass('placeholder');
            input.val(input.attr('placeholder'));
        }
    }).blur();
    $('[placeholder]').parents('form').submit(function() {
        $(this).find('[placeholder]').each(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
            }
        })
    });	
	
	$('.f-slider ul').bxSlider({
		mode: 'horizontal',
		adaptiveHeight: true,
		auto: false,		
		pager: true,
		pagerCustom: '#bx-pager',
		auto: false,
		infiniteLoop: false,
		hideControlOnEnd: true

	});
	$('#bx-pager').bxSlider({
		mode: 'horizontal',
		controls: true,
		auto: false,
		slideWidth: 158,
		minSlides: 4,
		maxSlides: 6,
		moveSlides: 1,
		pager: false,
		auto: false,
		infiniteLoop: false,
		hideControlOnEnd: true
	});
	
	
	//equal height
	equalheight = function(container){

	var currentTallest = 0,
		 currentRowStart = 0,
		 rowDivs = new Array(),
		 $el,
		 topPosition = 0;
	 $(container).each(function() {

	   $el = $(this);
	   $($el).height('auto')
	   topPostion = $el.position().top;

	   if (currentRowStart != topPostion) {
		 for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		   rowDivs[currentDiv].height(currentTallest);
		 }
		 rowDivs.length = 0; // empty the array
		 currentRowStart = topPostion;
		 currentTallest = $el.height();
		 rowDivs.push($el);
	   } else {
		 rowDivs.push($el);
		 currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
	  }
	   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		 rowDivs[currentDiv].height(currentTallest);
	   }
	 });
	}
	
	//
	$(window).load(function() {
		setTimeout(function() {
			//equalheight('.cols .new-cont');
		}, 100);
		setTimeout(function() {
			$(".tabs").tabs(".panes > div");
		}, 100);
				
	});
	
	function showDiv() {
		if ($(window).scrollTop() > 100 && $('#header').data('positioned') == 'false') {
			$("#header").hide().css({"position": "fixed"}).fadeIn(0).data('positioned', 'true');
			$("#header").addClass('fix');
		}
		else if ($(window).scrollTop() <= 100 && $('#header').data('positioned') == 'true') {
			$("#header").fadeOut(0, function() {
				$(this).css({"position": "absolute"}).show();
				$("#header").removeClass('fix');
			}).data('positioned', 'false');
		}
	}
	$(window).load(showDiv);
	$(window).scroll(showDiv);
	$('#header').data('positioned', 'false');
	
	$(function(){$.fn.scrollToTop=function(){$(this).hide().removeAttr("href");if($(window).scrollTop()!="0"){$(this).fadeIn("slow")}var scrollDiv=$(this);$(window).scroll(function(){if($(window).scrollTop()=="0"){$(scrollDiv).fadeOut("slow")}else{$(scrollDiv).fadeIn("slow")}});$(this).click(function(){$("html, body").animate({scrollTop:0},"slow")})}});
	$(function() {
		$("#toTop").scrollToTop();
	}); 
	
	$('.check input,.userAc.subscription input,select').styler();
	$('.check-box input').styler();
	
	//slider	
	$('.bxslider').bxSlider({
		mode: 'vertical',
		auto: false,
		minSlides: 3,
		maxSlides: 3,
		slideMargin: 0
	});	
	
	//
	$(".scroll").mCustomScrollbar();
	$('.sub-drop').hide();
	$('.sub li.active .sub-drop').show();
	
	$(".nav li a").each(function() {
		if ($(this).next().length > 0) {
			$(this).parent().addClass("parent");
		};
	});
	$(".nav > li.parent").hover(
        function () {
            $('.cov').show(0);
			$('.sub li:first-child').addClass('active');
			$('.sub li:first-child').find('.sub-drop').show();
        });
	$(".nav > li.parent").mouseleave(
        function () {
            $('.cov').hide(0);
        });
	
	$(".sub li.parent").click(
        function () {           
            $('.sub li').not(this).removeClass('active');
            $(this).parent('.sub').find('.sub-drop').not($(this).children('.sub-drop')).hide();
			$(this).find('.sub-drop').show();
            $(this).addClass('active');
			return false;
        });
	//
	$(".open-block").click(
        function () {           
			$('.more-photo-list').slideToggle();
			$('.userAc article header .back').toggle();
			$('.user-wall-photos-page .userAc article header .dalete-albom').toggle().toggleClass("show");
			return false;
        });
		
		
	$(".open-block").toggle(function() {
		$('.more-photo-list').slideDown();
		//$(this).text('скрыть все');
	 }, function() {
		$('.more-photo-list').slideUp();
		//$(this).text('показать все');
	});		
	//
	$(".more-text").click(
        function () {           
			$('.long-text').toggle();
			$('.shot-text').hide();
			$('.long-text').show();
			return false;
        });
	//
	$(".more-text2").click(
        function () {           
			$('.long-text').toggle();
			$('.long-text').hide();
			$('.shot-text').show();
			return false;
        });
	//
	$('.news .show-more').click(function(){
        $(this).prev('.hidden').slideDown(300);
		$(this).parent().parent().parent('.cont-block').find('.side-block').find('.hidden').slideDown(300);
		return false;
    });	
	$('.center .show-more').click(function(){
        $(this).prev('.hidden').slideDown(300);
		$(this).parent().parent('.cont-block').find('.side-block').find('.hidden').slideDown(300);
		return false;
    });	
	
	$('.show-more-news').click(function(){
        $(this).prev('.listLongNews').find('.hidden').slideDown(300);
		return false;
    });
	
/*	$('.more-comms').click(function(){
        $(this).parents('.userAc').find('.hidden-comms').slideDown(0);
		$(this).parents('.comments-wall').removeClass('hide');
		$(this).hide();
		$('.more-comms').hide();
		return false;
    });*/
    $('.more-comms').click(function(){
        var $articles = $(this).parents('.userAc').find('.hide-children')
            .removeClass('hide-children')
            .children('.wrapInsertedComment')
            .slideDown(0);
        $(this).parents('.comments-wall').removeClass('hide');
        $(this).hide();
        $('.more-comms').hide();
        return false;
    });

	$('.userHoverPopup .close').click(function(){
        $(this).parent('.userHoverPopup').fadeOut(300);
		return false;
    });	
	
	var
		n_counter = $('.numb .num').text().replace(/\D/g,""),
		num_digits = n_counter.length,
		element = '<p>',
		digits = n_counter.split('');

	for (var i = 0; i < num_digits; i++) {
		element += '<span class="contador-dig-' + i + '">' + digits[i] + '</span>';
	}
	$('.numb .num').html(element + '</p>');
	
	$('.numb').each(function() {		
		var asd = $(this);		
		asd.find('span.minus').click(function() {
			for (var i = num_digits; i >= 1; i--) {
				var
					$digit = $('.num').find('.contador-dig-'+(i-1)),
					digit = parseInt($digit.text(),10);
				if (0 === digit) {
					$digit.animate({
						top: '40px',
						}, 200, function(){
							$(this).text('9');
							$(this).animate({
								top: '-40px',
								},0,function(){
									$(this).animate({
										top: '0px',
									}, 200);
							});
					});
				} else {
					$digit.animate({
						top: '40px',
						}, 200, function(){
							var d = parseInt($(this).text(),10);
							$(this).text(--d);
							$(this).animate({
								top: '-40px',
								},0,function(){
									$(this).animate({
										top: '0px',
									}, 200);
							});
					});
					break;
				}
			}
		});
		
		asd.find('span.plus').click(function() {
			for (var i = num_digits; i >= 1; i--) {
				var
					$digit = $('.num').find('.contador-dig-'+(i-1)),
					digit = parseInt($digit.text(),10);
				if (9 === digit) {
					$digit.animate({
						top: '-40px',
						}, 200, function(){
							$(this).text('0');
							$(this).animate({
								top: '40px',
								},0,function(){
									$(this).animate({
										top: '0px',
									}, 200);
							});
					});
				} else {
					$digit.animate({
						top: '-40px',
						}, 200, function(){
							var d = parseInt($(this).text(),10);
							$(this).text(++d);
							$(this).animate({
								top: '40px',
								},0,function(){
									$(this).animate({
										top: '0px',
									}, 200);
							});
					});
					break;
				}
			}
		});
		
	});
	
    	
	//		
	$('#open-pop').click(function(){
        $('#overlay').fadeIn(300);
        $('#b-service-popup').fadeIn(300);
	});	
	$('#overlay, #b-service-popup .close').click(function(){
        $('#overlay').fadeOut(300);
        $('#b-service-popup').fadeOut(300);
	});	
	
	
	$('.close').click(function(){$(this).parents('.top-panel').fadeOut(), $('#header').toggleClass('top'); return false;});
	
	$('.closeDrop-downMenu').click(function(){$(this).parents('.drop').fadeOut(); return false;});
	
	$("#ex-one").organicTabs();
	$("#ex-one2-1, #ex-one2-2, #ex-one2-3").organicTabs2();
	
	//
	
	$(".new-tabs").tabs(".new-pane");
	$(".wall-filter").tabs(".wall-panes > div");
	$(".tabs-news").tabs(".tabs-news-panes > div");
	
	//top menu
	$(".top-panel li a").each(function() {
		if ($(this).next().length > 0) {
			$(this).parent().addClass("parent");
		};
	});
	$(".top-panel .parent > a").click(
        function () {           
            $('.top-panel .parent').not($(this).parent('li')).removeClass('active');
            $('.top-panel .parent ul').not($(this).next('ul')).hide();
			$(this).next('ul').toggle();
            $(this).parent('li').toggleClass('active');
			return false;
    });
	

	//
	$('.fancy').fancybox({
	   scrolling : 'auto'
	});
    
	$('.fancy-noclose').fancybox({
	   scrolling : 'auto',
	    'closeBtn' : false
	});
	
	$(document).mouseup(function (e){
		var container = $(".top-panel .parent");
		if (!container.is(e.target) 
			&& container.has(e.target).length === 0) 
		{
			$('.top-panel .parent ul').hide();
		}
	});
	
	
    /***/
    var photoSlider = $('#photo-slder').bxSlider({
        pager : false,
        slideWidth : 650
    });
    
    
    $('.photo-cell a.fancy, .add-photo-list li a.fancy').fancybox({
	   scrolling : 'auto',
       'closeBtn' : false,
       beforeShow: function(){
            $('#container').addClass('blur');
            $('.fancybox-skin').addClass('shadow');
            photoSlider.reloadSlider();
        },
        afterClose: function(){
            $('#container').removeClass('blur');
            $('.fancybox-skin').removeClass('shadow');
        }
	});
    
    $('.close-photo, .f-close, .close-fancy').click (function() {
        parent.$.fancybox.close();
		return false;
    });
    
    
    $('.action-gift.reply').click (function(e) {
            e.preventDefault();
            $('.pane.gift .wall-filter').tabs(".pane.gift .wall-panes > div",{
                initialIndex : '2'
            });
            $('.user-gift').show();
    });
	
    $('.userAc.photo-box:first-child').show();
    
	
    $('.present-content.fancy').click(function(){
        if ($('.user-gift').is(':visible')){
            $('#single-gift .userAc article').show();
        }
        else{
            $('#single-gift .userAc article').hide();
        }
    });
    
    $('.pane.gift .wall-filter li').click (function() {
            $('.user-gift').hide();
            $('.my-single-gift').hide();
            $('.my-gift-list').show();
    });  
       
       
      $('.userAc.my-gift .my-gift-list .gift-card-img').click (function(e){
        e.preventDefault();
        $('.userAc.my-gift .my-gift-list').hide();
        $('.my-single-gift').show();
     });
     
      $('.userAc.single .my-gift-list .gift-card-img').click (function(e){
        e.preventDefault();
        $('.userAc.single .my-gift-list').hide();
        $('.my-single-gift').show();
     });
     
     
     $('.action-gift.list').click (function(e){
        e.preventDefault();
        $('.my-gift-list').show();
        $('.my-single-gift').hide();
     });
	 
	$('.comments-wall .blue-link2').click (function(e){
        var $this = $(this);
        $this
            .closest('.comments-wall')
                .addClass('hide')
                .find('.hidden-comms')
                    .hide()
                .end()
                .find('.comment-wrapper')
                    .addClass('hide-children');
		$('.more-comms').show();
		return false; 
    });	
	 
	$('.c-block input').styler(); 
	
	
	$('.edit-settings').click (function(e){
        e.preventDefault();
        $(this).next('.sett-drop').slideToggle(200);
    });
	
	$(".tabs-bord").tabs(".communs");
	
    $(".scrolled").mCustomScrollbar();
	$(".mail-tabs").tabs(".mail-pane");
	
	$('.info-box .edit-info').click (function(e){
        e.preventDefault();
        $(this).parent().parent().find('.sett-drop').show();
        $(this).parent().parent().find('.saved-info').hide();
        $(this).parent().parent().addClass('active');
    });
	
	$('.cancel-edit').click (function(e){
        e.preventDefault();
        $(this).parent().parent('.sett-drop').hide();
        $(this).parent().parent().prev('.saved-info').show();
		$(this).parent().parent().removeClass('active');
    });
	
	$('.btn-save').click (function(e){
        $('.save-mess').fadeIn(500).delay(1000).fadeOut(500);		
	});
	
	$('.search-opener h2').click (function(e){
        $(this).parent().next('.hidden').slideToggle(300);		
		$(this).toggleClass('active');
	});
	
	$('.fr-wait .close').click (function(e){
        $(this).parent('li').hide();	
		return false;		
	});
	
	$('.publ-auth').click (function(e){
        $(this).text('Републикация');	
        $(this).prev('.t-inp').val('Ian Bougracewitz').removeClass('placeholder');	
		return false;		
	});
	
	//
	//var ww = document.body.clientWidth;
	var ww = $('body').width();
	$(document).ready(function() {
		adjustMenu();
	});	
	$(window).bind('resize orientationchange', function() {
		//ww = document.body.clientWidth;
		ww = $('body').width();
		adjustMenu();		
	});
	
	var adjustMenu = function() {
		if (ww < 1024) {
			$('.top-panel').css('left', -$('#container').scrollLeft() + "px");
			$('#header').css('left', -$('#container').scrollLeft() + "px");
		}
		else if (ww >= 1024) {
			$('.top-panel').css('left', "0");
			$('#header').css('left', "50%");
		}
		
		$(window).scroll(function() {
			if (ww < 1024) {
				$('.top-panel').css('left', -$('body').scrollLeft() + "px");
				$('#header.fix').css('left', -$('body').scrollLeft() + "px");
				
				var isAndroid = navigator.userAgent.toLowerCase().indexOf("android");
				if(isAndroid > -1)
				{
					$('.top-panel').css('left', -$('#container').scrollLeft() + "px");
					$('#header.fix').css('left', -$('#container').scrollLeft() + "px");
				} 
			}
			else if (ww >= 1024) {
				$('.top-panel').css('left', "0");
				$('#header').css('left', "50%");
			}	
		});
		
		
	}
	
	
	
	
	$('.scroll-pep').mCustomScrollbar();
	
	$('.scroll-pep .people_cell').click (function(e){
        $(this).toggleClass('active');	
		return false;		
	});
	
	//
	$('.close-message').click (function(e){
        $(this).parent('.drop-message').fadeOut(300);	
		return false;		
	});
	
    ~function(){
        var $formsBlock = $('<div/>')
                .addClass('comm-respond commentnews active'),
            $respond = $('article.respond')
                .appendTo($formsBlock),
            $complain = $('article.complain')
                .appendTo($formsBlock),
            $wrappedArticle = null;

        function showForm($article, type){
            hideForm($wrappedArticle);
            
            var depth = +$article.data('depth');

            if (type === 'complain') {
                $respond.hide();
                $complain.show();
            } else {
                $complain.hide();
                $respond.show();
            }

            $wrappedArticle = $article;
            $formsBlock
                .insertAfter($article)
                .prepend($article)
                .css('margin-left', -18 * Math.min(6, depth) + 'px');
        }
        function hideForm(){
            if (!$wrappedArticle) {
                return;
            }
            $wrappedArticle
                .insertBefore($formsBlock);
            $formsBlock.detach();
        }

        $('.complain-link, .respond-link').click(function(e){
            e.preventDefault();
            var $this = $(this),
                $article = $this.closest('article');
            showForm($article, $this.hasClass('respond-link') ? 'respond' : 'complain');
        });

        $formsBlock.find('.cancel-link').click(function(e){
            e.preventDefault();
            hideForm();
        });
    }();
	
	$(".userAc.photo-box .open-block").toggle(function() {
		$(this).text('Изменить название');
	 }, function() {
		$(this).text('Редактировать');
	});	
	
	
});